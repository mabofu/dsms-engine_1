RPM_NAME=dsms-engine
RPM_VERSION=V1.0.0
RPM_RELEASE=V1.0
RPM_ARCH=x86_64
RPM_BUILD_DIR=$(shell pwd)/rpmbuild
RPM_SPEC_FILE=$(RPM_BUILD_DIR)/SPECS/$(RPM_NAME).spec


.PHONY: clean build

clean:
	rm -rf target
	rm -rf $(RPM_BUILD_DIR)
	mvn clean
build: clean
	mvn clean package -DskipTests
	mkdir -p $(RPM_BUILD_DIR)/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
	mkdir -p $(RPM_BUILD_DIR)/BUILD/dsms-engine/
	cp sql/dsms-engine_init.sql $(RPM_BUILD_DIR)/BUILD/dsms-engine/
	cp build/dsms-engine.service $(RPM_BUILD_DIR)/BUILD/dsms-engine/
	cp build/start.sh $(RPM_BUILD_DIR)/BUILD/dsms-engine/
	find dsms-engine-application/target -type f -name "*.jar" -exec cp {} $(RPM_BUILD_DIR)/BUILD/dsms-engine/ \;

	sed -e "s/@RPM_NAME@/$(RPM_NAME)/g" \
	        -e "s/@RPM_VERSION@/$(RPM_VERSION)/g" \
	        -e "s/@RPM_RELEASE@/$(RPM_RELEASE)/g" \
	        -e "s/@RPM_ARCH@/$(RPM_ARCH)/g" \
        	-e "s|@RPM_BUILD_DIR@|$(RPM_BUILD_DIR)|g" \
	        build/dsms-engine.spec > $(RPM_SPEC_FILE)


	rpmbuild -bb $(RPM_SPEC_FILE)

	mkdir target

	find $(RPM_BUILD_DIR)/RPMS -type f -name "*.rpm" -exec cp {} ./target \;

	rm -rf $(RPM_BUILD_DIR)



