/*
    dsms-engine 单元测试初始化ddl
*/

CREATE TABLE `sm_user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(12) NOT NULL DEFAULT '' COMMENT '用户名',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态(0-正常，1-停用)',
  `password` varchar(100) NOT NULL DEFAULT '' COMMENT '密码',
  `last_login_ip` varchar(50)  DEFAULT '' COMMENT '最后登录IP',
  `last_login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`id`) 
) ;

INSERT INTO sm_user(id, username, status, password, last_login_ip, last_login_date) VALUES(1, 'admin', 0, '{noop}R0ck90', '127.0.0.1', NULL);

CREATE TABLE `sm_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `code` varchar(30)  NOT NULL COMMENT '角色编码',
  `name` varchar(30)  NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`) 
) ;

INSERT INTO sm_role(id, code,name) VALUES(1, 'admin','管理员');

CREATE TABLE `sm_user_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增编号',
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) 
) ;

INSERT INTO sm_user_role(id, user_id,role_id) VALUES(1,1,1);


CREATE TABLE `sm_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `name` varchar(50)  NOT NULL COMMENT '菜单名称',
  `path` varchar(200)  NULL DEFAULT '' COMMENT '路由地址',
  `parent_id` bigint NOT NULL DEFAULT 0 COMMENT '父菜单ID',
  PRIMARY KEY (`id`)
) ;

CREATE TABLE `sm_role_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增编号',
  `role_id` int NOT NULL COMMENT '角色ID',
  `menu_id` int NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) 
);

CREATE TABLE `cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `cluster_name` varchar(32)  NOT NULL COMMENT '集群名称',
  `cluster_address` varchar(32)  NOT NULL COMMENT '集群服务地址',
  `cluster_port` int(11) NOT NULL COMMENT '集群服务端口',
  `bind_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-未绑定，1-已绑定',
  `auth_key` varchar(36)  NOT NULL COMMENT '集群服务认证key',
  `admin_key` varchar(50)  NOT NULL COMMENT 'admin key',
  `fsid` char(36)  NOT NULL COMMENT '集群唯一fsid',
  `create_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_cluster_address_cluster_port` (`cluster_address`,`cluster_port`)
);

CREATE TABLE `task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `task_name` varchar(32) NOT NULL DEFAULT '' COMMENT '任务名称',
  `task_type` varchar(32) NOT NULL DEFAULT '' COMMENT '任务类型',
  `task_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '任务状态（0-未开始，1-队列中，2-执行中，3-完成，4-失败）',
  `task_message` varchar(128) NOT NULL DEFAULT '' COMMENT '任务信息',
  `task_param` varchar(255) NOT NULL DEFAULT '' COMMENT '任务参数',
  `task_start_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '任务开始时间',
  `task_end_time` datetime COMMENT '任务结束时间',
  `task_error_message` varchar(512) COMMENT '任务执行错误信息',
  PRIMARY KEY (`id`)
);

CREATE TABLE `step` (
  `id` int NOT NULL AUTO_INCREMENT,
  `task_id` int NOT NULL COMMENT '主任务id',
  `step_name` varchar(32) NOT NULL DEFAULT '' COMMENT '子任务名称',
  `step_type` varchar(32) NOT NULL DEFAULT '' COMMENT '子任务类型',
  `step_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '子任务状态（0-未开始，1-队列中，2-执行中，3-完成，4-失败）',
  `step_message` varchar(128) NOT NULL DEFAULT '' COMMENT '子任务信息',
  `step_param` varchar(255) NOT NULL DEFAULT '' COMMENT '子任务参数',
  `step_start_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '子任务开始时间',
  `step_end_time` datetime COMMENT '子任务结束时间',
  `step_error_message` varchar(512) COMMENT '子任务执行错误信息',
  PRIMARY KEY (`id`)
);


CREATE TABLE `alert_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警规则id',
  `rule_metric` varchar(32)  NOT NULL DEFAULT '' COMMENT '告警规则监控项',
  `rule_promql` varchar(512)  NOT NULL DEFAULT '' COMMENT '告警规则表达式',
  `rule_compare_type` int(11) NOT NULL DEFAULT 0 COMMENT '告警规则比较运算符（0-相等，1-大于，-1-小于，2-不相等）',
  `rule_threshold` varchar(32)  NOT NULL DEFAULT '0' COMMENT '告警规则阈值',
  `rule_threshold_init` varchar(32)  NOT NULL DEFAULT '0' COMMENT '告警规则阈值初始值',
  `rule_times` int(11) NOT NULL DEFAULT 0 COMMENT '告警规则周期',
  `rule_times_init` int(11) NOT NULL DEFAULT 0 COMMENT '告警规则周期初始值',
  `rule_module` varchar(32)  NOT NULL DEFAULT '0' COMMENT '告警规则模块',
  `rule_level` int(11) NOT NULL DEFAULT 0 COMMENT '告警级别（0-提示、1-一般、2-重要、3-紧急）',
  `rule_level_init` int(11) NOT NULL DEFAULT 0 COMMENT '告警级别初始值',
  `last_check_time` datetime NOT NULL DEFAULT current_timestamp() COMMENT '告警规则检测时间',
  PRIMARY KEY (`id`)
);

CREATE TABLE `alert_apprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警通知id',
  `type` tinyint NOT NULL DEFAULT 0 COMMENT '告警通知类型(0-email,1-sms)',
  `smtp_host` varchar(32)  NOT NULL DEFAULT '' COMMENT '告警通知smtp服务器地址',
  `smtp_port` int(11)  NOT NULL DEFAULT 25 COMMENT '告警通知smtp服务器端口',
  `smtp_username` varchar(32)  NOT NULL DEFAULT '' COMMENT '告警通知smtp用户名',
  `smtp_password` varchar(32)  NOT NULL DEFAULT '' COMMENT '告警通知smtp密码',
  `smtp_ssl` tinyint  NOT NULL DEFAULT 0 COMMENT '告警通知smtp是否开启ssl(0-关闭,1-开启)',
  `email_from` varchar(32)  NOT NULL DEFAULT '' COMMENT '告警通知发送人邮箱',
  `sms_provider` varchar(16)  NOT NULL DEFAULT '' COMMENT '告警通知短信服务商',
  `sms_provider_host` varchar(32)  NOT NULL DEFAULT '' COMMENT '告警通知短信服务域名',
  `sms_region` varchar(16) NOT NULL DEFAULT '' COMMENT '告警通知所在地域对应id',
  `sms_access_id` varchar(64) NOT NULL DEFAULT '' COMMENT '告警短信平台访问密钥id',
  `sms_access_secret` varchar(64) NOT NULL DEFAULT '' COMMENT '告警短信平台访问密钥key',
  `sms_app_id` varchar(32) NOT NULL DEFAULT '' COMMENT '告警短信平台应用id',
  `sms_sign_name` varchar(16) NOT NULL DEFAULT '' COMMENT '告警通知短信签名',
  `sms_template_code` varchar(16) NOT NULL DEFAULT '' COMMENT '告警通知短信模板code',
  `status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '告警通知状态(0-关闭、1-开启)',
  PRIMARY KEY (`id`)
);

CREATE TABLE `alert_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '告警联系人id',
  `contact_type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '告警联系人类型(0-email,1-sms)',
  `contact_name` varchar(32) NOT NULL DEFAULT '' COMMENT '告警联系人名称',
  `contact_account` varchar(32) NOT NULL DEFAULT '' COMMENT '告警联系人帐号',
  PRIMARY KEY (`id`)
);