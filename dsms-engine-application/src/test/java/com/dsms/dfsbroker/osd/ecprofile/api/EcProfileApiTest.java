/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.ecprofile.api;

import com.dsms.ClusterProperties;
import com.dsms.common.constant.RemoteResponseStatusEnum;
import com.dsms.common.remotecall.model.FailedDetail;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.osd.ecprofile.model.dto.EcProfileDto;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
public class EcProfileApiTest {
    private static RemoteRequest request;
    @Autowired
    EcProfileApi ecProfileApi;
    @Autowired
    CommonApi commonApi;
    @Autowired
    private ClusterProperties properties;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void createEcProfile() throws Throwable {
        //normal create ec-profile
        EcProfileDto ecProfileDtoNormal = new EcProfileDto("apiTest_root", 2, 2, 1);
        Assertions.assertThatCode(() -> ecProfileApi.createEcProfile(request, ecProfileDtoNormal)).doesNotThrowAnyException();

        //create ec-profile with exception
        EcProfileDto ecProfileDtoException = new EcProfileDto("apiTest_exception", 0, 0, 1);
        RemoteResponse exceptionResponse = ecProfileApi.createEcProfile(request, ecProfileDtoException);
        RemoteResponse createEcProfileResult = ecProfileApi.getCreateEcProfileResult(RemoteCallUtil.generateRemoteRequest(), exceptionResponse.getId());
        assertEquals(createEcProfileResult.getState(), RemoteResponseStatusEnum.FAILED.getMessage());
        List<FailedDetail> failed = createEcProfileResult.getFailed();
        String outs = failed.get(0).getOuts();
        assertNotNull(outs);
    }

}
