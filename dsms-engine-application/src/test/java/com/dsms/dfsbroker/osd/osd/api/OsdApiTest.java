/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.osd.api;


import com.dsms.ClusterProperties;
import com.dsms.common.constant.CrushFailureDomainEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class OsdApiTest {
    private static RemoteRequest request;
    @Autowired
    private OsdApi osdApi;
    @Autowired
    private ClusterProperties properties;

    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }

    @Test
    void getOsdById() {
        Assertions.assertThatCode(() -> osdApi.getOsdById(request, 0)).doesNotThrowAnyException();
    }

    @Test
    void updateOsdBucket() {
        Assertions.assertThatCode(() -> osdApi.updateOsdBucketRequest(request, 0, CrushFailureDomainEnum.HOST, "node1")).doesNotThrowAnyException();
    }
}