/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.cluster.api;


import com.dsms.ClusterProperties;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.cluster.model.remote.ServerResult;
import com.dsms.modules.util.RemoteCallUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@EnabledIf(expression = "${dsms.storage.enable}", loadContext = true)
class ClusterApiTest {
    @Autowired
    private ClusterApi clusterApi;
    private static RemoteRequest request;
    @Autowired
    private ClusterProperties properties;


    @BeforeEach
    public void setUp() {
        Cluster cluster = properties.getTestCluster();
        RemoteCallUtil.updateRemoteFixedParam(cluster);
        request = RemoteCallUtil.generateRemoteRequest();
    }
    @Test
    void getClusterVersion() {
        Cluster clusterVersion = clusterApi.getClusterVersion(request);
        Assertions.assertNotNull(clusterVersion.getNodes());
    }

    @Test
    void getServer() throws Throwable {
        List<ServerResult> server = clusterApi.getServer(request);
        assertThat(server, hasItems(hasProperty("hostname", is(request.getRemoteFixedParam().getHost()))));
    }


}