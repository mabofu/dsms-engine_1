/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.alert.entity.AlertContact;
import com.dsms.modules.alert.service.IAlertContactService;
import com.dsms.modules.mail.model.MailMessage;
import com.dsms.modules.mail.service.IMailSendService;
import com.dsms.modules.sms.model.SmsSendMessage;
import com.dsms.modules.sms.service.ISmsSendService;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AlertAppriseServiceImplTest {

    @Mock
    private IAlertContactService mockAlertContactService;
    @Mock
    private IMailSendService mockMailService;
    @Mock
    private ISmsSendService mockSmsSendService;

    @InjectMocks
    private AlertAppriseServiceImpl alertAppriseServiceImplUnderTest;

    @Test
    void testSaveOrUpdateAlertApprise() {
        // Setup
        final AlertApprise alertApprise = new AlertApprise();
        alertApprise.setId(0);
        alertApprise.setType(0);
        alertApprise.setSmtpHost("smtpHost");
        alertApprise.setSmtpPort(0);
        alertApprise.setSmtpUsername("smtpUsername");
        alertApprise.setSmtpPassword("smtpPassword");
        alertApprise.setEmailFrom("emailFrom");
        alertApprise.setSmsProvider("smsProvider");
        alertApprise.setSmsAccessId("smsAccessId");
        alertApprise.setSmsAccessSecret("smsAccessSecret");
        alertApprise.setSmsSignName("smsSignName");
        alertApprise.setSmsTemplateCode("smsTemplate");
        alertApprise.setStatus(0);
        final AlertContact alertContact = new AlertContact();
        alertContact.setContactType(0);
        alertContact.setContactAccount("contactAccount");
        alertApprise.setContacts(List.of(alertContact));

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.saveOrUpdateAlertApprise(alertApprise);

        // Verify the results
        assertThat(result).isFalse();
        verify(mockMailService).initMailAccount();
        verify(mockSmsSendService).initSmsClient();
    }

    @Test
    void testSendTestEmail() {
        // Setup
        when(mockAlertContactService.listObjs(any(LambdaQueryWrapper.class), any(Function.class))).thenReturn(List.of("value"));

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.sendTestEmail();

        // Verify the results
        assertThat(result).isFalse();
        verify(mockMailService).sendMail(new MailMessage("smtpUsername", "title", "content", List.of("value")));
    }

    @Test
    void testSendTestEmail_IAlertContactServiceReturnsNoItems() {
        // Setup
        when(mockAlertContactService.listObjs(any(LambdaQueryWrapper.class), any(Function.class))).thenReturn(Collections.emptyList());

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.sendTestEmail();

        // Verify the results
        assertThat(result).isFalse();
        verify(mockMailService).sendMail(new MailMessage("smtpUsername", "title", "content", List.of("value")));
    }

    @Test
    void testSendTestSms() {
        // Setup
        when(mockAlertContactService.listObjs(any(LambdaQueryWrapper.class), any(Function.class))).thenReturn(List.of("value"));

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.sendTestSms();

        // Verify the results
        assertThat(result).isFalse();
        verify(mockSmsSendService).doSendSms(new SmsSendMessage());
    }

    @Test
    void testSendTestSms_IAlertContactServiceReturnsNoItems() {
        // Setup
        when(mockAlertContactService.listObjs(any(LambdaQueryWrapper.class), any(Function.class))).thenReturn(Collections.emptyList());

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.sendTestSms();

        // Verify the results
        assertThat(result).isFalse();
        verify(mockSmsSendService).doSendSms(new SmsSendMessage());
    }

    @Test
    void testListNotifyAlertApprise() {
        // Setup
        final AlertApprise alertApprise = new AlertApprise();
        alertApprise.setId(0);
        alertApprise.setType(0);
        alertApprise.setSmtpHost("smtpHost");
        alertApprise.setSmtpPort(0);
        alertApprise.setSmtpUsername("smtpUsername");
        alertApprise.setSmtpPassword("smtpPassword");
        alertApprise.setEmailFrom("emailFrom");
        alertApprise.setSmsProvider("smsProvider");
        alertApprise.setSmsAccessId("smsAccessId");
        alertApprise.setSmsAccessSecret("smsAccessSecret");
        alertApprise.setSmsSignName("smsSignName");
        alertApprise.setSmsTemplateCode("smsTemplate");
        alertApprise.setStatus(0);
        final AlertContact alertContact = new AlertContact();
        alertContact.setContactType(0);
        alertContact.setContactAccount("contactAccount");
        alertApprise.setContacts(List.of(alertContact));
        final List<AlertApprise> expectedResult = List.of(alertApprise);

        // Configure IAlertContactService.list(...).
        final AlertContact alertContact1 = new AlertContact();
        alertContact1.setId(0);
        alertContact1.setContactType(0);
        alertContact1.setContactName("contactName");
        alertContact1.setContactAccount("contactAccount");
        final List<AlertContact> alertContacts = List.of(alertContact1);
        when(mockAlertContactService.list(any(LambdaQueryWrapper.class))).thenReturn(alertContacts);

        // Run the test
        final List<AlertApprise> result = alertAppriseServiceImplUnderTest.listNotifyAlertApprise();

        // Verify the results
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testListNotifyAlertApprise_IAlertContactServiceReturnsNoItems() {
        // Setup
        when(mockAlertContactService.list(any(LambdaQueryWrapper.class))).thenReturn(Collections.emptyList());

        // Run the test
        final List<AlertApprise> result = alertAppriseServiceImplUnderTest.listNotifyAlertApprise();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testValidateAlertApprise() {
        // Setup
        final AlertApprise alertApprise = new AlertApprise();
        alertApprise.setId(0);
        alertApprise.setType(0);
        alertApprise.setSmtpHost("smtpHost");
        alertApprise.setSmtpPort(0);
        alertApprise.setSmtpUsername("smtpUsername");
        alertApprise.setSmtpPassword("smtpPassword");
        alertApprise.setEmailFrom("emailFrom");
        alertApprise.setSmsProvider("smsProvider");
        alertApprise.setSmsAccessId("smsAccessId");
        alertApprise.setSmsAccessSecret("smsAccessSecret");
        alertApprise.setSmsSignName("smsSignName");
        alertApprise.setSmsTemplateCode("smsTemplate");
        alertApprise.setStatus(0);
        final AlertContact alertContact = new AlertContact();
        alertContact.setContactType(0);
        alertContact.setContactAccount("contactAccount");
        alertApprise.setContacts(List.of(alertContact));

        // Run the test
        final boolean result = alertAppriseServiceImplUnderTest.validateAlertApprise(alertApprise);

        // Verify the results
        assertThat(result).isFalse();
    }
}
