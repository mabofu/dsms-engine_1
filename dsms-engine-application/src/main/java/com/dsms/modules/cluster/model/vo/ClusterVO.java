/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.modules.cluster.model.vo;

import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.GeneralParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

@ApiModel(value = "关联集群 Request VO", description = "集群信息")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClusterVO {

    @ApiModelProperty(value = "集群名称", required = true, example = "dev")
    @NotBlank(message = "集群名称不能为空")
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.CLUSTER)
    private String clusterName;

    @ApiModelProperty(value = "集群服务地址", required = true, example = "127.0.0.1/node1")
    @NotBlank(message = "集群服务地址不能为空")
    @Length(max = 32, message = "集群服务地址长度不能超过32")
    private String clusterAddress;

    @ApiModelProperty(value = "集群服务端口", required = true, example = "0")
    @Min(value = 0, message = "集群服务端口参数值不可小于0")
    @Max(value = 65535, message = "集群服务端口参数值不可大于65535")
    private Integer clusterPort;

    @ApiModelProperty(value = "集群服务认证key", required = true, example = "key")
    @NotBlank(message = "集群服务认证key不能为空")
    @Length(max = 36, message = "集群服务认证key长度不能超过36")
    private String authKey;

}
