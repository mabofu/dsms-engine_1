/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagedir.model.vo;

import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.GeneralParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "创建存储目录VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StorageDirCreateVO {
    @ApiModelProperty(value = "文件系统名称", required = true, example = "fs")
    @NotBlank(message = "文件系统名称不能为空")
    private String fsName;

    @ApiModelProperty(value = "存储目录名称", required = true, example = "fsDir")
    @NotBlank(message = "存储目录名称不能为空")
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.STORAGE_DIR)
    private String storageDirName;

}
