/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.modules.node.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "节点新增磁盘VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NodeAddVO {

    @ApiModelProperty(value = "节点名称", required = true, example = "node1")
    @NotBlank(message = "节点名称不能为空")
    private String nodeName;
    @ApiModelProperty(value = "磁盘路径", required = true, example = "/dev/sda")
    @NotBlank(message = "磁盘路径不能为空")
    private String devicePath;


}
