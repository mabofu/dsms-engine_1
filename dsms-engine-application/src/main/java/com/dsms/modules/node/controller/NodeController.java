/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.node.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.Result;
import com.dsms.dfsbroker.node.model.Node;
import com.dsms.dfsbroker.node.service.INodeService;
import com.dsms.modules.node.model.vo.NodeAddVO;
import com.dsms.modules.node.model.vo.NodePageVO;
import com.dsms.modules.node.model.vo.NodeQueryVO;
import com.dsms.modules.node.model.vo.NodeRemoveVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/node")
@Api(tags = "节点池模块")
public class NodeController {

    @Autowired
    private INodeService nodeService;

    @ApiOperation("节点池模块-节点池信息列表")
    @PostMapping("/list")
    public Result<Page<Node>> list(@Validated @RequestBody NodePageVO nodePageVO) {
        Page<Node> nodePage = nodeService.page(nodePageVO);

        return Result.OK(nodePage);
    }

    @ApiOperation("节点池模块-获取节点详细信息")
    @PostMapping("/get")
    public Result<Node> info(@Validated @RequestBody NodeQueryVO nodeVO) {
        return Result.OK(nodeService.getNodeInfo(nodeVO.getNodeName()));
    }

    @ApiOperation("节点池模块-节点管理磁盘")
    @PostMapping("/add_osd")
    public Result<Boolean> addOsd(@Validated @RequestBody NodeAddVO nodeAddVO) {
        return Result.OK(nodeService.addOsd(nodeAddVO.getNodeName(), nodeAddVO.getDevicePath()));
    }

    @ApiOperation("节点池模块-节点移除磁盘")
    @PostMapping("/remove_osd")
    public Result<Boolean> removeOsd(@Validated @RequestBody NodeRemoveVO nodeRemoveVO) {
        return Result.OK(nodeService.removeOsd(nodeRemoveVO.getOsdId()));
    }

}
