/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.modules.util;

import com.dsms.common.constant.SystemConst;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.common.remotecall.model.RemoteFixedParam;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.cluster.model.Cluster;
import com.dsms.dfsbroker.cluster.service.IClusterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RemoteCallUtil implements ApplicationRunner {
    @Autowired
    private IClusterService iClusterService;

    public static RemoteFixedParam remoteFixedParam = null;

    public static RemoteRequest generateRemoteRequest() {
        return new RemoteRequest(remoteFixedParam);
    }

    public static void updateRemoteFixedParam(Cluster cluster) {
        remoteFixedParam = new RemoteFixedParam();
        remoteFixedParam.setScheme(SystemConst.HTTPS_SCHEME);
        remoteFixedParam.setHost(cluster.getClusterAddress());
        remoteFixedParam.setPort(cluster.getClusterPort());
        remoteFixedParam.setUsername(SystemConst.DSMS_STORAGE_USER);
        remoteFixedParam.setPassword(cluster.getAuthKey());
    }

    @Override
    public void run(ApplicationArguments args) {
        try {
            Cluster initCluster = iClusterService.getCurrentBindCluster();
            updateRemoteFixedParam(initCluster);
        } catch (DsmsEngineException e) {
            log.warn("The manager not bind a cluster,{}", e.getMessage());
        }
    }
}
