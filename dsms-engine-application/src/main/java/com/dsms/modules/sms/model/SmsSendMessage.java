/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms.model;

import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Sms send message
 */
@Data
public class SmsSendMessage {

    /**
     * 短信日志编号
     */
    @NotNull(message = "短信日志编号不能为空")
    private Long logId;

    /**
     * target mobile number
     */
    @NotNull(message = "手机号不能为空")
    private String[] mobiles;

    /**
     * 短信渠道编号
     */
    @NotNull(message = "短信服务商不能为空")
    private String smsProvider;

    /**
     * sms signature information
     */
    @NotNull(message = "短信签名不能为空")
    private String smsSignName;

    /**
     * sms template code
     */
    @NotNull(message = "短信模板编号不能为空")
    private String smsTemplate;

    /**
     * sms template params
     */
    private List<Map<String, Object>> templateParams;

}
