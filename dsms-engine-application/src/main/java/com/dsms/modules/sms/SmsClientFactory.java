/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.sms;


import com.dsms.modules.alert.entity.AlertApprise;

/**
 * Sms client factory
 */
public interface SmsClientFactory {

    /**
     * Get sms provider client
     *
     * @param smsProvider sms provider
     * @return sms Client
     */
    SmsClient getSmsClient(String smsProvider);

    /**
     * Create or update sms provider client
     *
     * @param alertApprise sms provider properties
     */
    void createOrUpdateSmsClient(AlertApprise alertApprise);

}
