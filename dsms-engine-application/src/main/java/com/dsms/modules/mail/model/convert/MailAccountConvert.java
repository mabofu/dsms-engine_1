/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.mail.model.convert;

import cn.hutool.extra.mail.MailAccount;
import com.dsms.common.constant.CommonEnum;
import com.dsms.modules.alert.entity.AlertApprise;

public class MailAccountConvert {

    public static MailAccount convert(AlertApprise alertApprise) {
        if (alertApprise == null) {
            return null;
        }

        MailAccount mailAccount = new MailAccount();

        mailAccount.setFrom(alertApprise.getEmailFrom());
        mailAccount.setAuth(true);
        mailAccount.setUser(alertApprise.getSmtpUsername());
        mailAccount.setPass(alertApprise.getSmtpPassword());
        mailAccount.setHost(alertApprise.getSmtpHost());
        mailAccount.setPort(alertApprise.getSmtpPort());
        mailAccount.setSslEnable(alertApprise.getSmtpSsl().equals(CommonEnum.ENABLE.getCode()));

        return mailAccount;
    }

}
