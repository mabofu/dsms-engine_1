/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.filesystem.task;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.dsms.common.constant.StepTypeEnum;
import com.dsms.common.constant.TaskStatusEnum;
import com.dsms.common.constant.TaskTypeEnum;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.common.taskmanager.TaskException;
import com.dsms.common.taskmanager.TaskStrategy;
import com.dsms.common.taskmanager.model.Step;
import com.dsms.common.taskmanager.model.Task;
import com.dsms.common.taskmanager.service.IStepService;
import com.dsms.common.taskmanager.service.ITaskService;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.filesystem.api.FileSystemApi;
import com.dsms.dfsbroker.filesystem.model.dto.FileSystemDTO;
import com.dsms.dfsbroker.storagedir.api.StorageDirApi;
import com.dsms.dfsbroker.storagedir.model.StorageDir;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;
import com.dsms.dfsbroker.storagedir.request.StorageDirDeAuthRequest;
import com.dsms.dfsbroker.storagedir.service.IStorageDirService;
import com.dsms.dfsbroker.storagepool.service.IStoragePoolService;
import com.dsms.modules.util.RemoteCallUtil;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


@Slf4j
@Service(TaskTypeEnum.TypeConstants.REMOVE_FS)
public class RemoveFsTask implements TaskStrategy {

    private static final int RETRY_TIME = 3;

    @Autowired
    private FileSystemApi fileSystemApi;

    @Autowired
    private StorageDirApi storageDirApi;

    @Autowired
    private IStoragePoolService storagePoolService;

    @Autowired
    @Lazy
    private IStorageDirService storageDirService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private CommonApi commonApi;

    @Autowired
    private IStepService stepService;

    @Override
    public Task execute(Task task) {
        String taskParam = task.getTaskParam();
        if (ObjectUtils.isEmpty(taskParam)) {
            task.setTaskEndTime(LocalDateTime.now());
            task.setTaskErrorMessage("task parameter is null");
            task.setTaskStatus(TaskStatusEnum.FAIL.getStatus());
            return task;
        }
        FileSystemDTO fileSystemDTO = JSONUtil.toBean(taskParam, FileSystemDTO.class);


        Integer taskId = task.getId();
        List<Step> steps = stepService.list(new LambdaQueryWrapper<Step>().eq(Step::getTaskId, taskId));

        //0.delete file system auth id
        Step deleteFileSystemAuthId = steps.stream().filter((tmp) -> tmp.getStepType().equals(StepTypeEnum.DELETE_FILE_SYSTEM_AUTH.getType())).findFirst().orElse(new Step());
        executeStep(fileSystemDTO, deleteFileSystemAuthId);

        //1.bring the file system down step
        Step failFileSystemStep = steps.stream().filter((tmp) -> tmp.getStepType().equals(StepTypeEnum.FAIL_FILE_SYSTEM.getType())).findFirst().orElse(new Step());
        executeStep(fileSystemDTO, failFileSystemStep);

        //2.delete file system step
        Step deleteFileSystemStep = steps.stream().filter((tmp) -> tmp.getStepType().equals(StepTypeEnum.DELETE_FILE_SYSTEM.getType())).findFirst().orElse(new Step());
        executeStep(fileSystemDTO, deleteFileSystemStep);

        //3.delete file system step
        Step purgeUsedPool = steps.stream().filter((tmp) -> tmp.getStepType().equals(StepTypeEnum.PURGE_FILE_SYSTEM_POOL.getType())).findFirst().orElse(new Step());
        executeStep(fileSystemDTO, purgeUsedPool);

        task.setTaskStatus(TaskStatusEnum.FINISH.getStatus());

        return task;
    }

    private void executeStep(FileSystemDTO fileSystemDTO, Step step) {
        try {
            execute(fileSystemDTO, step);
        } catch (Throwable t) {
            log.error("execute step fail,fail message:{},step info:{}", t.getMessage(), JSON.toJSONString(step), t);
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
            step.setStepErrorMessage(StringUtils.hasText(step.getStepErrorMessage()) ? step.getStepErrorMessage() : t.getMessage());
            throw new TaskException(step.getStepErrorMessage());
        } finally {
            try {
                if (TaskStatusEnum.FAIL.getStatus() == step.getStepStatus() || TaskStatusEnum.FINISH.getStatus() == step.getStepStatus()) {
                    step.setStepEndTime(LocalDateTime.now());
                }
                stepService.updateById(step);
            } catch (Throwable t) {
                log.error("update step info fail,fail message:{},step info:{}", t.getMessage(), JSON.toJSONString(step), t);
            }
        }
    }

    public void execute(FileSystemDTO fileSystemDTO, Step step) throws Throwable {
        step.setStepStatus(TaskStatusEnum.EXECUTING.getStatus());
        RemoteResponse remoteResponse = null;
        if (StepTypeEnum.FAIL_FILE_SYSTEM.getType().equals(step.getStepType())) {
            remoteResponse = fileSystemApi.failFs(RemoteCallUtil.generateRemoteRequest(), fileSystemDTO);
        } else if (StepTypeEnum.DELETE_FILE_SYSTEM.getType().equals(step.getStepType())) {
            remoteResponse = fileSystemApi.removeFs(RemoteCallUtil.generateRemoteRequest(), fileSystemDTO);
        } else if (StepTypeEnum.PURGE_FILE_SYSTEM_POOL.getType().equals(step.getStepType())) {
            storagePoolService.purgePool(fileSystemDTO.getMetadata());
            storagePoolService.purgePool(fileSystemDTO.getData());
            step.setStepParam(JSON.toJSONString(fileSystemDTO));
            step.setStepStatus(TaskStatusEnum.FINISH.getStatus());
            return;
        } else if (StepTypeEnum.DELETE_FILE_SYSTEM_AUTH.getType().equals(step.getStepType())) {
            List<StorageDir> storageDirs = storageDirService.list();
            for (StorageDir storageDir : storageDirs) {
                if (fileSystemDTO.getFsName().equals(storageDir.getFsName())) {
                    StorageDirDTO storageDirDTO = new StorageDirDTO(storageDir.getFsName(), storageDir.getStorageDirName(), storageDir.getStorageDirAuthId());
                    step = deleteFileSystemAuth(storageDirDTO, step);
                }
            }
            return;
        }
        if (ObjectUtils.isEmpty(remoteResponse)) {
            throw new TaskException("execute " + step.getStepName() + " step fail");
        }
        boolean flag = true;
        while (flag) {
            step = stepService.getStepResponse(step, remoteResponse);
            if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                flag = false;
            }
        }

        if (!Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus())) {
            throw new TaskException(step.getStepErrorMessage());
        }
    }

    private Step deleteFileSystemAuth(StorageDirDTO storageDirDTO, Step step) {
        try {
            RemoteResponse remoteResponse = storageDirApi.storageDirDeAuthorize(RemoteCallUtil.generateRemoteRequest(), storageDirDTO);
            boolean flag = true;
            while (flag) {
                step = stepService.getStepResponse(step, remoteResponse);
                if (Objects.equals(step.getStepStatus(), TaskStatusEnum.FINISH.getStatus()) || Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                    flag = false;
                }
            }
            if (StepTypeEnum.DELETE_FILE_SYSTEM_AUTH.getType().equals(step.getStepType()) && Objects.equals(step.getStepStatus(), TaskStatusEnum.FAIL.getStatus())) {
                if (step.getStepErrorMessage().equals(String.format(StorageDirDeAuthRequest.AUTH_ALREADY_DELETE, storageDirDTO.getAuthId()))) {
                    step.setStepStatus(TaskStatusEnum.FINISH.getStatus());
                }
            }
        } catch (Throwable t) {
            step.setStepStatus(TaskStatusEnum.FAIL.getStatus());
            throw new TaskException("execute " + step.getStepName() + " step fail", t);
        }
        return step;
    }

    @Override
    public boolean validateTask(String[] validateParam) {
        return taskService.validateTaskMessageAndTaskType(validateParam[0]);
    }


}
