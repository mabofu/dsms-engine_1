/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.filesystem.model;

import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.GeneralParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "创建文件系统VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileSystemCreateVO {
    @ApiModelProperty(value = "文件系统名称", required = true, example = "fs")
    @NotBlank(message = "文件系统名称不能为空")
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.FILESYSTEM)
    private String fsName;
    @ApiModelProperty(value = "文件系统元数据存储池", required = true, example = "metadatapool")
    @NotBlank(message = "文件系统元数据存储池不能为空")
    private String metadataPool;
    @ApiModelProperty(value = "文件系统数据存储池", required = true, example = "datapool")
    @NotBlank(message = "文件系统数据存储池不能为空")
    private String dataPool;

}
