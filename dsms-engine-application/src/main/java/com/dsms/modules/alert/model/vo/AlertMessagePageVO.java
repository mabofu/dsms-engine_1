/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.model.vo;

import com.dsms.common.model.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Range;

@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "获取告警信息列表VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlertMessagePageVO extends PageParam {

    @ApiModelProperty(value = "告警级别（0-提示、1-一般、2-重要、3-紧急）", example = "0")
    @Range(min = 0, max = 3, message = "告警级别不存在")
    private Integer level;
    @ApiModelProperty(value = "告警信息状态（0-未读、1-已读）", example = "0")
    @Range(min = 0, max = 1, message = "告警信息状态不存在")
    private Integer status;
    @ApiModelProperty(value = "告警信息模块(cluster、node、devices)", example = "cluster")
    private String model;

}
