/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.controller;

import static com.dsms.common.constant.ResultCode.ALERT_CONTACT_EMAIL_ILLEGAL;
import static com.dsms.common.constant.ResultCode.ALERT_CONTACT_EXIST;
import static com.dsms.common.constant.ResultCode.ALERT_CONTACT_MOBILE_ILLEGAL;

import cn.hutool.core.lang.Validator;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dsms.common.constant.AlertAppriseTypeEnum;
import com.dsms.common.exception.DsmsEngineException;
import com.dsms.common.model.Result;
import com.dsms.modules.alert.entity.AlertContact;
import com.dsms.modules.alert.model.vo.AlertContactDeleteVO;
import com.dsms.modules.alert.service.IAlertContactService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/alertcontact")
@Api(tags = "告警联系人模块")
public class AlertContactController {

    @Autowired
    private IAlertContactService alertContactService;

    @ApiOperation("告警联系人模块-新增")
    @PostMapping("/create")
    public Result<Boolean> create(@Validated @RequestBody AlertContact alertContact) {
        if (AlertAppriseTypeEnum.SMS.getCode().equals(alertContact.getContactType())) {
           Validator.validateMobile(alertContact.getContactAccount(), ALERT_CONTACT_MOBILE_ILLEGAL.getMessage());
        } else if (AlertAppriseTypeEnum.EMAIL.getCode().equals(alertContact.getContactType())) {
            Validator.validateEmail(alertContact.getContactAccount(),ALERT_CONTACT_EMAIL_ILLEGAL.getMessage());
        }

        long count = alertContactService.count(Wrappers.lambdaQuery(AlertContact.class).eq(AlertContact::getContactAccount, alertContact.getContactAccount()));
        if (count > 0) {
            throw new DsmsEngineException(ALERT_CONTACT_EXIST);
        }
        return Result.OK(alertContactService.save(alertContact));
    }

    @ApiOperation("告警联系人模块-删除")
    @PostMapping("/delete")
    public Result<Boolean> delete(@Validated @RequestBody AlertContactDeleteVO alertContactDelete) {

        return Result.OK(alertContactService.removeById(alertContactDelete.getContactId()));
    }


}
