/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.Result;
import com.dsms.modules.alert.entity.AlertMessage;
import com.dsms.modules.alert.model.vo.AlertMessageManageVO;
import com.dsms.modules.alert.model.vo.AlertMessagePageVO;
import com.dsms.modules.alert.model.vo.AlertMessageQueryVO;
import com.dsms.modules.alert.service.IAlertMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/alertmessage")
@Api(tags = "告警信息模块")
public class AlertMessageController {

    @Autowired
    public IAlertMessageService alertMessageService;

    @ApiOperation("告警信息模块-获取告警信息列表")
    @PostMapping("/list")
    public Result<Page<AlertMessage>> list(@Validated @RequestBody AlertMessagePageVO alertMessagePageVO) {
        return Result.OK(alertMessageService.listAlertMessage(alertMessagePageVO));
    }

    @ApiOperation("告警信息模块-获取单条告警信息")
    @PostMapping("/get")
    public Result<AlertMessage> get(@Validated @RequestBody AlertMessageQueryVO alertMessageQueryVO) {
        return Result.OK(alertMessageService.getAlertMessage(alertMessageQueryVO));
    }

    @ApiOperation("告警信息模块-确认告警信息")
    @PostMapping("/confirm_message")
    public Result<Boolean> confirmMessage(@Validated @RequestBody AlertMessageManageVO alertMessageManageVO) {
        return Result.OK(alertMessageService.confirmMessage(alertMessageManageVO));
    }

    @ApiOperation("告警信息模块-删除告警信息")
    @PostMapping("/delete_message")
    public Result<Boolean> deleteMessage(@Validated @RequestBody AlertMessageManageVO alertMessageManageVO) {
        return Result.OK(alertMessageService.deleteMessage(alertMessageManageVO));
    }

    @ApiOperation("告警信息模块-获取未读告警信息")
    @PostMapping("/get_unconfirmed_message_num")
    public Result<Long> getUnconfirmedMessageNum() {
        return Result.OK(alertMessageService.getUnconfirmedMessageNum());
    }
}
