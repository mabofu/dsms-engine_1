/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ApiModel(value = "告警策略更新VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AlertRuleUpdateVO {
    @ApiModelProperty(value = "告警规则id", example = "1")
    @NotNull(message = "告警规则id不能为空")
    private Integer ruleId;

    @ApiModelProperty(value = "告警规则阈值", example = "1")
    @NotNull(message = "告警规则阈值不能为空")
    @Min(value = 0,message = "告警规则阈值最小值为0")
    private String ruleThreshold;

    @ApiModelProperty(value = "告警规则持续时间", example = "1")
    @NotNull(message = "告警规则持续时间不能为空")
    @Min(value = 1, message = "告警规则持续时间最小值为1")
    @Max(value = 3600, message = "告警规则持续时间最大值为3600")
    private Integer ruleTimes;

    @ApiModelProperty(value = "告警规则级别", example = "1")
    @NotNull(message = "告警规则级别不能为空")
    private Integer ruleLevel;

}
