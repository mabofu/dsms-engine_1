/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.util.List;

@TableName("alert_apprise")
@ApiModel(value = "AlertApprise对象", description = "告警通知表")
@Data
public class AlertApprise implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("告警通知id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("告警通知类型(0-email,1-sms)")
    @Range(min = 0, max = 1, message = "告警通知类型不存在")
    private Integer type;

    @ApiModelProperty("告警通知smtp服务器地址")
    @Length(max = 32, message = "告警通知smtp服务器地址长度不能超过32")
    private String smtpHost;

    @ApiModelProperty("告警通知smtp服务器端口")
    @Min(value = 0, message = "告警通知smtp服务器端口参数值不可小于0")
    @Max(value = 65535, message = "告警通知smtp服务器端口参数值不可大于65535")
    private Integer smtpPort;

    @ApiModelProperty("告警通知smtp用户名")
    @Length(max = 32, message = "告警通知smtp用户名长度不能超过32")
    private String smtpUsername;

    @ApiModelProperty("告警通知smtp密码")
    @Length(max = 32, message = "告警通知smtp密码长度不能超过32")
    private String smtpPassword;

    @ApiModelProperty("告警通知smtp ssl开关(0-关闭,1-开启)")
    private Integer smtpSsl;

    @ApiModelProperty("告警通知发送人邮箱")
    @Email(message = "告警通知发送人邮箱格式验证失败")
    @Length(max = 32, message = "告警通知发送人邮箱长度不能超过32")
    private String emailFrom;

    @ApiModelProperty("告警通知短信服务商")
    @Length(max = 16, message = "告警通知短信服务商长度不能超过16")
    private String smsProvider;

    @ApiModelProperty("告警通知短信服务域名")
    @Length(max = 32, message = "告警通知短信服务域名长度不能超过32")
    private String smsProviderHost;

    @ApiModelProperty("告警通知所在地域对应id")
    @Length(max = 16, message = "告警通知所在地域对应id长度不能超过16")
    private String smsRegion;

    @ApiModelProperty("告警短信平台访问密钥id")
    @Length(max = 64, message = "告警短信平台访问密钥id长度不可超过64")
    private String smsAccessId;

    @ApiModelProperty("告警短信平台访问密钥key")
    @Length(max = 64, message = "告警短信平台访问密钥key长度不能超过64")
    private String smsAccessSecret;

    @ApiModelProperty("告警短信平台应用id")
    @Length(max = 32, message = "告警短信平台应用id长度不能超过32")
    private String smsAppId;

    @ApiModelProperty("告警通知短信签名")
    @Length(max = 16, message = "告警通知短信签名长度不能超过16")
    private String smsSignName;

    @ApiModelProperty("告警通知短信模板code")
    @Length(max = 16, message = "告警通知短信模板code长度不能超过16")
    private String smsTemplateCode;

    @ApiModelProperty("告警通知状态(0-关闭、1-开启)")
    @Range(min = 0, max = 1, message = "告警通知状态不存在")
    private Integer status;

    @TableField(exist = false)
    private List<AlertContact> contacts;

}
