/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.constant.AlertRuleModuleEnum;
import com.dsms.common.constant.AlertRuleTypeEnum;
import com.dsms.common.model.Result;
import com.dsms.modules.alert.entity.AlertRule;
import com.dsms.modules.alert.model.vo.AlertRulePageVO;
import com.dsms.modules.alert.model.vo.AlertRuleQueryVO;
import com.dsms.modules.alert.model.vo.AlertRuleUpdateVO;
import com.dsms.modules.alert.service.IAlertRuleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/alertrule")
@Api(tags = "告警规则模块")
public class AlertRuleController {

    @Autowired
    public IAlertRuleService alertRuleService;

    @ApiOperation("告警规则模块-获取规则列表")
    @PostMapping("/list")
    public Result<Page<AlertRule>> list(@Validated @RequestBody AlertRulePageVO alertRulePageVO) {

        return Result.OK(alertRuleService.getAlertRulePage(alertRulePageVO));
    }

    @ApiOperation("告警策略模块-单个告警策略")
    @PostMapping("/get")
    public Result<AlertRule> get(@Validated @RequestBody AlertRuleQueryVO alertRuleQueryVO) {
        AlertRule alertRule = alertRuleService.getById(alertRuleQueryVO.getRuleId());
        alertRule.setRuleName(AlertRuleTypeEnum.getalertRuleTypeEnum(alertRule.getRuleMetric()).getName());
        alertRule.setRuleModule(AlertRuleModuleEnum.getAlertRuleModuleEnum(alertRule.getRuleModule()).getModuleName());

        return Result.OK(alertRule);
    }

    @ApiOperation("告警策略模块-更新告警策略")
    @PostMapping("/update_rule")
    public Result<Boolean> update(@Validated @RequestBody AlertRuleUpdateVO alertRuleUpdateVO) {

        return Result.OK(alertRuleService.updateAlertRule(alertRuleUpdateVO));
    }


    @ApiOperation("告警策略模块-重置告警策略")
    @PostMapping("/reset_rule")
    public Result<Boolean> reset(@Validated @RequestBody AlertRuleQueryVO alertRuleQueryVO) {

        return Result.OK(alertRuleService.resetAlertRule(alertRuleQueryVO));
    }
}
