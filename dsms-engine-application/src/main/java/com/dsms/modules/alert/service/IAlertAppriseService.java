/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.alert.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dsms.common.constant.AlertRuleLevelEnum;
import com.dsms.common.constant.AlertRuleModuleEnum;
import com.dsms.modules.alert.entity.AlertApprise;
import com.dsms.modules.alert.entity.AlertMessage;

import java.util.List;

/**
 * AlertApprise operation service
 */
public interface IAlertAppriseService extends IService<AlertApprise> {

    boolean saveOrUpdateAlertApprise(AlertApprise alertApprise);

    boolean sendTestEmail();

    boolean sendTestSms();

    List<AlertApprise> listNotifyAlertApprise();

    /**
     * send custom template warning information
     *
     * @param alertMessages template warning information
     */
    void notifyContact(List<AlertMessage> alertMessages);

    /**
     * send custom warning information
     *
     * @param message             custom warning information
     * @param alertRuleModuleEnum alert rule module
     * @param alertRuleLevelEnum  alert rule level
     */
    void notifyContact(String message, AlertRuleModuleEnum alertRuleModuleEnum, AlertRuleLevelEnum alertRuleLevelEnum);
}
