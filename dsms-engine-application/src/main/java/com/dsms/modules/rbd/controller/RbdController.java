/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.rbd.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.Result;
import com.dsms.dfsbroker.rbd.model.Rbd;
import com.dsms.dfsbroker.rbd.model.dto.RbdCreateDTO;
import com.dsms.dfsbroker.rbd.model.dto.RbdDeleteDTO;
import com.dsms.dfsbroker.rbd.model.dto.RbdGetDTO;
import com.dsms.dfsbroker.rbd.model.dto.RbdUpdateDTO;
import com.dsms.dfsbroker.rbd.service.IRbdService;
import com.dsms.modules.rbd.model.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/volume")
@Api(tags = "存储卷模块")
public class RbdController {
    @Autowired
    private IRbdService rbdService;

    @ApiOperation("存储卷模块-获取指定存储卷")
    @PostMapping("/get")
    public Result<Rbd> get(@Validated @RequestBody RbdGetVO rbdGetVO) {
        RbdGetDTO rbdGetDTO = new RbdGetDTO(rbdGetVO.getPoolName(), rbdGetVO.getRbdName());
        Rbd rbd = rbdService.get(rbdGetDTO);
        return Result.OK(rbd);
    }

    @ApiOperation("存储卷模块-获取存储卷列表")
    @PostMapping("/list")
    public Result<Page<Rbd>> list(@Validated @RequestBody RbdListVO rbdListVO) {
        Page<Rbd> rbdList = rbdService.list(rbdListVO);
        return Result.OK(rbdList);
    }

    @ApiOperation("存储卷模块-创建存储卷")
    @PostMapping("/create_rbd")
    public Result<Boolean> create(@Validated @RequestBody RbdCreateVO rbdCreateVO) {
        RbdCreateDTO rbdCreateDTO = new RbdCreateDTO(
                rbdCreateVO.getPoolName(),
                rbdCreateVO.getRbdName(),
                rbdCreateVO.getRbdSize(),
                rbdCreateVO.getDataPoolName(),
                rbdCreateVO.getBasedOnEra()
        );
        return Result.OK(rbdService.create(rbdCreateDTO));
    }

    @ApiOperation("存储卷模块-删除存储卷")
    @PostMapping("/delete_rbd")
    public Result<Boolean> delete(@Validated @RequestBody RbdDeleteVO rbdDeleteVO) {
        RbdDeleteDTO rbdDeleteDTO = new RbdDeleteDTO(
                rbdDeleteVO.getPoolName(),
                rbdDeleteVO.getRbdName()
        );
        return Result.OK(rbdService.delete(rbdDeleteDTO));
    }

    @ApiOperation("存储卷模块-存储卷扩缩容")
    @PostMapping("/update_rbd")
    public Result<Boolean> update(@Validated @RequestBody RbdUpdateVO rbdUpdateVO) {
        RbdUpdateDTO rbdUpdateDTO = new RbdUpdateDTO(
                rbdUpdateVO.getPoolName(),
                rbdUpdateVO.getRbdName(),
                rbdUpdateVO.getRbdSize()
        );
        return Result.OK(rbdService.update(rbdUpdateDTO));
    }

    @ApiOperation("存储卷模块-下载证书")
    @PostMapping("/download_key")
    public Result<Object> downloadKey() {
        return rbdService.downloadKey();
    }
}
