/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagepool.model.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@ApiModel(value = "存储池节点管理VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoragePoolNodeManageVO {
    @ApiModelProperty(value = "存储池名称", example = "pool1")
    @NotBlank(message = "存储池名称不能为空")
    private String poolName;

    //use to remove single node of storage pool
    @ApiModelProperty(value = "节点名称", example = "node1")
    private String nodeName;

    //use to add nodes of storage pool
    @ApiModelProperty(value = "节点名称列表", example = "['node1','node2']")
    @JsonProperty("nodeNames")
    private List<String> nodeNames;
}
