/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.storagepool.model.vo;

import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.GeneralParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel(value = "创建存储池VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoragePoolCreateVO {
    @ApiModelProperty(value = "存储池名称", required = true, example = "pool1")
    @NotBlank(message = "存储池名称不能为空")
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.STORAGE_POOL)
    private String poolName;
    //the type of storage pool: 1 indicates the replicated pool ,3 indicates the erasure pool
    @ApiModelProperty(value = "存储池类型", required = true, example = "1(replicated) 3(erasure)")
    @NotNull(message = "存储池类型不能为空")
    private int poolType;
    @ApiModelProperty(value = "副本数", example = "3")
    @Min(value = 2, message = "副本数不可小于2")
    private Integer replicaNum;
    @ApiModelProperty(value = "存储池pg数", required = true, example = "32")
    @NotNull(message = "pg数不能为空")
    private int pgNum;
    @ApiModelProperty(value = "存储池pgp数", required = true, example = "32")
    @NotNull(message = "pgp数不能为空")
    private int pgpNum;
    @ApiModelProperty(value = "纠删码池配置文件", example = "32")
    @Valid
    private EcProfile ecProfile;

    @Data
    @NoArgsConstructor
    public static class EcProfile {
        @ApiModelProperty(value = "纠删码池数据块数", example = "2")
        @Min(value = 2, message = "数据块数不可小于2")
        private Integer dataChunks;
        @ApiModelProperty(value = "纠删码池校验块数", example = "1")
        @Min(value = 1, message = "校验块数不可小于1")
        private Integer codeChunks;
        //the type of plugin to create ec-profile: 0 indicates the "jerasure" ,1 indicates "isa"
        @ApiModelProperty(value = "创建配置文件的插件类型", example = "0(jerasure) 1(isa)")
        private int plugin;

    }
}
