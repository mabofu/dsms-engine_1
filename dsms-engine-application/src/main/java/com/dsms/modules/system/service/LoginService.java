/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.system.service;

import com.dsms.common.model.Result;
import com.dsms.modules.system.model.SmUser;
import com.dsms.modules.system.model.vo.LoginReqVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface LoginService {
    /**
     * login with username and password
     *
     * @param loginReqVO loginVO
     */
    Result<SmUser> login(HttpServletRequest request, HttpServletResponse response, LoginReqVO loginReqVO);

    /**
     * logout
     */
    Result<String> logout(HttpServletRequest request);

    /**
     * get the current logged-on user
     */
    Result<SmUser> getLoginUser();
}
