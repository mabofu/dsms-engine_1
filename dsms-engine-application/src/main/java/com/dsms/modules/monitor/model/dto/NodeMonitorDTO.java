/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "节点性能监控信息", description = "集群状态监控信息")
public class NodeMonitorDTO {
    @ApiModelProperty("cpu使用信息")
    private Map<String, double[][]> cpu;
    @ApiModelProperty("内存使用信息")
    private MemoryDTO memory;
    @ApiModelProperty("网络监控信息")
    private NetWorkDto netWork;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class MemoryDTO {
        @ApiModelProperty("内存总大小")
        private double[][] totalMemory;
        @ApiModelProperty("已使用内存大小")
        private double[][] usedMemory;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class NetWorkDto {
        @ApiModelProperty("网络负载情况")
        private DataDTO load;
        @ApiModelProperty("网络丢包率")
        private DataDTO dropRate;
        @ApiModelProperty("网络错误率")
        private DataDTO errorRate;
        @ApiModelProperty("网络重传率")
        private double[][] retransRate;

        @NoArgsConstructor
        @AllArgsConstructor
        @Data
        public static class DataDTO {
            @ApiModelProperty("网络‘接收’的监控数据")
            private Map<String, double[][]> receive;
            @ApiModelProperty("网络‘发送’的监控数据")
            private Map<String, double[][]> transmit;
        }
    }
}
