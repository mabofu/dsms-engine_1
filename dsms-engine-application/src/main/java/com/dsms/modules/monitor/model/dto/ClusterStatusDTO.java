/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "集群状态监控信息", description = "集群状态监控信息")
public class ClusterStatusDTO {

    @ApiModelProperty("osd信息")
    private OsdDTO osd;
    @ApiModelProperty("mon信息")
    private MonDTO mon;
    @ApiModelProperty("mgr信息")
    private MgrDTO mgr;
    @ApiModelProperty("节点信息")
    private NodeDTO node;
    @ApiModelProperty("cpu使用百分比")
    private Double cpuUsage;
    @ApiModelProperty("内存使用百分比")
    private Double memoryUsage;
    @ApiModelProperty("存储使用百分比")
    private Double capacityUsage;

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class OsdDTO {
        @ApiModelProperty("osd总数")
        private Integer osdNum;
        @ApiModelProperty("正常osd数")
        private Integer osdNormalNum;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class MonDTO {
        @ApiModelProperty("mon总数")
        private Integer monNum;
        @ApiModelProperty("mon quorum列表")
        private List<String> quorum;
        @ApiModelProperty("mon down列表")
        private List<String> down;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class MgrDTO {
        @ApiModelProperty("mgr leader")
        private String mgrLeader;
        @ApiModelProperty("mgr standby")
        private List<String> standby;
        @ApiModelProperty("mgr standby数量")
        private Integer standbyNum;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class NodeDTO {
        @ApiModelProperty("在线节点")
        private List<String> online;
        @ApiModelProperty("离线节点")
        private List<String> offline;
    }

}
