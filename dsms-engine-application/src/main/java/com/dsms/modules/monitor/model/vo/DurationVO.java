/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@ApiModel(value = "监控数据起止时间、查询步进参数VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DurationVO {
    @ApiModelProperty(value = "开始时间", required = true, example = "2023-02-20 14:45:16")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @NotNull(message = "开始时间不能为空")
    private LocalDateTime start;
    @ApiModelProperty(value = "结束时间", required = true, example = "2023-02-20 14:46:16")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @NotNull(message = "结束时间不能为空")
    private LocalDateTime end;

    @ApiModelProperty(value = "step", example = "0")
    private int step;

    public long getStartTimeStamp() {
        return start.toInstant(ZoneOffset.of("+8")).toEpochMilli() / 1000;
    }

    public long getEndTimeStamp() {
        return end.toInstant(ZoneOffset.of("+8")).toEpochMilli() / 1000;
    }

    public String getStep() {
        //The step calculation method is consistent with prometheus
        if (step == 0) {
            return String.valueOf(Math.max((int) ((getEndTimeStamp() - getStartTimeStamp()) / 250), 1));
        }
        return String.valueOf(step);
    }
}
