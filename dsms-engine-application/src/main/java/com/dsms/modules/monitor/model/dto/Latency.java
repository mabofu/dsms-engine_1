/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.monitor.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * The latency of the two stages of osd storage data in dsms-storage
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Latency {
    /*
     * In dsms-storage, the client's write request is first sent to the OSD node where the original copy of the data is located.
     * On that node, the request is applied to the data replica to ensure that the data is written correctly.
     * This process is called "apply". During the apply phase, a copy of the data may not have been broadcast to other OSD nodes.
     * */
    private double[][] apply;
    /*
     * After the apply phase, data copies are propagated to other OSD nodes in the dsms-storage for redundant replication of data.
     * A write operation is considered "commit" once the data replicas have confirmed the written was successful on a sufficient number of OSD nodes
     * */
    private double[][] commit;
}
