/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.modules.task.model;

import com.dsms.common.model.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@ApiModel(value = "磁盘分页VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskPageVO extends PageParam {

    /**
     * Some tasks need to be rolled back after failure, so they will enter the "rollback" state.
     * When the rollback is successful, it will enter the "rollback successful" state;
     * if not successful, it will return to the "rollback" state and wait for the next rollback.
     */
    @ApiModelProperty(value = "任务状态（0-未开始；1-队列中；2-执行中；3-完成；4-任务失败，无需回滚；5-任务失败，回滚中；6-任务失败,回滚成功）", example = "0")
    private List<Integer> taskStatus;

}
