/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.cluster.model.remote;

import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@NoArgsConstructor
@Data
public class StatusResult {

    @JsonProperty("fsid")
    private String fsid;
    //cluster health status
    @JsonProperty("health")
    private HealthDTO health;
    //the quorum node number of mon
    @JsonProperty("quorum")
    private List<Integer> quorum;
    //the quorum node name of mon
    @JsonProperty("quorum_names")
    private List<String> quorumNames;
    //the clusters' mon status map
    @JsonProperty("monmap")
    private MonmapDTO monmap;
    //the clusters' osd status map
    @JsonProperty("osdmap")
    private OsdmapDTO osdmap;
    //the clusters' mgr status map
    @JsonProperty("mgrmap")
    private MgrmapDTO mgrmap;

    @NoArgsConstructor
    @Data
    public static class HealthDTO {
        //cluster status. ex. HEALTH_OK
        @JsonProperty("status")
        private String status;
        //the reasons of unhealthy
        @JsonProperty("checks")
        private Map<String, JSONObject> checks;
    }

    @NoArgsConstructor
    @Data
    public static class MonmapDTO {
        @JsonProperty("num_mons")
        private Integer numMons;
    }

    @NoArgsConstructor
    @Data
    public static class OsdmapDTO {
        @JsonProperty("num_osds")
        private Integer numOsds;
        @JsonProperty("num_up_osds")
        private Integer numUpOsds;
        @JsonProperty("num_in_osds")
        private Integer numInOsds;
    }


    @NoArgsConstructor
    @Data
    public static class MgrmapDTO {
        @JsonProperty("num_standbys")
        private Integer numStandbys;
        @JsonProperty("leader")
        private String leader;
    }

    @NoArgsConstructor
    @Data
    public static class HealthCheckDTO {
        @JsonProperty("severity")
        private String severity;
        @JsonProperty("summary")
        private HealthCheckSummaryDTO summary;
        @JsonProperty("muted")
        private boolean muted;
    }
    @NoArgsConstructor
    @Data
    public static class HealthCheckSummaryDTO {
        @JsonProperty("message")
        private String message;
        @JsonProperty("count")
        private Integer count;
    }


}
