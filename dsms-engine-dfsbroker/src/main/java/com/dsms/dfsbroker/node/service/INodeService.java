/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.PageParam;
import com.dsms.dfsbroker.node.model.Node;

import java.util.List;

public interface INodeService {

    /**
     * get dsms-storage all node info
     *
     * @return nodes info list
     */
    List<Node> list();

    /**
     * get dsms-storage node info with page param
     *
     * @return nodes info list
     */
    Page<Node> page(PageParam pageParam);

    /**
     * get dsms-storage special node info
     *
     * @param nodeName node name
     * @return node info
     */
    Node getNodeInfo(String nodeName);

    /**
     * node add osd
     *
     * @param nodeName   node name
     * @param devicePath node disk path
     */
    boolean addOsd(String nodeName, String devicePath);

    /**
     * node remove osd
     *
     * @param osdId osd id
     */
    boolean removeOsd(Integer osdId);

    boolean validateNode(String nodeName);

    boolean validateDeviceAvailable(String nodeName, String devicePath);

    boolean validateOsdId(Integer osdId);
}
