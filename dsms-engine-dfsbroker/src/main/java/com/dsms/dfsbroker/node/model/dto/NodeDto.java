/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.node.model.dto;

import com.dsms.dfsbroker.node.model.Device;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class NodeDto {

    @JsonProperty("nodeId")
    private String nodeId;
    @JsonProperty("nodeName")
    private String nodeName;
    //the number of usable osd on node
    @JsonProperty("usedDevice")
    private Integer usedDevice;
    //the number of all osd on node
    @JsonProperty("totalDevice")
    private Integer totalDevice;
    @JsonProperty("nodeDevices")
    private List<Device> nodeDevices;

    public NodeDto(String nodeId, String nodeName, List<Device> nodeDevices) {
        this.nodeId = nodeId;
        this.nodeName = nodeName;
        this.nodeDevices = nodeDevices;
    }

    public NodeDto(String nodeId, String nodeName, Integer usedDevice, Integer totalDevice, List<Device> nodeDevices) {
        this.nodeId = nodeId;
        this.nodeName = nodeName;
        this.nodeDevices = nodeDevices;
        this.usedDevice = usedDevice;
        this.totalDevice = totalDevice;
    }
}
