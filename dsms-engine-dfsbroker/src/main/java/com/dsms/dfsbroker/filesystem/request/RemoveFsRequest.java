/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.filesystem.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import com.dsms.dfsbroker.filesystem.model.dto.FileSystemDTO;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

public class RemoveFsRequest extends RemoteRequest {
    public RemoveFsRequest(FileSystemDTO fileSystem) {
        Map<String, Object> requestParam = new HashMap<>(4);
        requestParam.put("prefix", "fs rm");
        requestParam.put("fs_name", fileSystem.getFsName());
        requestParam.put("yes_i_really_mean_it", true);

        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
