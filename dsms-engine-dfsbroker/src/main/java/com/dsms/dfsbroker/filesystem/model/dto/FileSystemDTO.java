/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.filesystem.model.dto;


import com.dsms.common.constant.NameSchemeEnum;
import com.dsms.common.validator.DsmsValidateGroup;
import com.dsms.common.validator.GeneralParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileSystemDTO {

    @NotBlank(message = "文件系统名称不能为空", groups = {DsmsValidateGroup.Remove.class, DsmsValidateGroup.Create.class})
    @GeneralParam(nameSchemeEnum = NameSchemeEnum.FILESYSTEM)
    private String fsName;
    @NotBlank(message = "文件系统元数据存储池不能为空", groups = {DsmsValidateGroup.Create.class})
    private String metadata;
    @NotBlank(message = "文件系统数据存储池不能为空", groups = {DsmsValidateGroup.Create.class})
    private String data;

    private String requestId;

    public FileSystemDTO(String fsName) {
        this.fsName = fsName;
    }
}
