/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.model;


import com.dsms.dfsbroker.storagedir.model.remote.SubvolumeInfoResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

@Data
@ApiModel(value = "StorageDir对象", description = "存储目录")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StorageDir {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("文件系统名称")
    private String fsName;

    @ApiModelProperty("存储目录名称")
    private String storageDirName;

    @ApiModelProperty("存储目录路径")
    private String storageDirPath;

    @ApiModelProperty("存储目录用量")
    private Long storageDirUsedSize;

    @ApiModelProperty("存储目录容量")
    private Long storageDirTotalSize;

    @ApiModelProperty("存储目录帐号")
    private String storageDirAuthId;

    @ApiModelProperty("存储目录帐号密钥")
    private String storageDirAuthKey;

    @ApiModelProperty("存储目录服务地址")
    private String storageDirMonAddrs;

    public static StorageDir subvolumeInfoResponseParseStorageDir(SubvolumeInfoResponse subvolumeInfo) {
        if (ObjectUtils.isEmpty(subvolumeInfo)) {
            return new StorageDir();
        }
        String storageDirMonAddrs = String.join(",", subvolumeInfo.getMonAddrs());
        return StorageDir.builder()
                .storageDirPath(subvolumeInfo.getPath())
                .storageDirUsedSize(subvolumeInfo.getBytesUsed())
                .storageDirMonAddrs(storageDirMonAddrs)
                .build();
    }

}
