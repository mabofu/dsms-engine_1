/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagedir.api;

import com.alibaba.fastjson2.JSONArray;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.storagedir.model.dto.StorageDirDTO;
import com.dsms.dfsbroker.storagedir.model.remote.SubvolumeAuthListResponse;
import com.dsms.dfsbroker.storagedir.model.remote.SubvolumeInfoResponse;
import com.dsms.dfsbroker.storagedir.model.remote.SubvolumeLsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Component
@Slf4j
public class StorageDirApi {

    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * List subvolumes name
     */
    public List<SubvolumeLsResponse> getStorageDirList(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructStorageDirLsRequest(remoteRequest, storageDirDTO);
        SubvolumeLsResponse[] subvolumeLsResponses = commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, SubvolumeLsResponse[].class);
        return Arrays.stream(subvolumeLsResponses).collect(Collectors.toList());
    }

    /**
     * Get the information of a CephFS subvolume in a volume
     */
    public SubvolumeInfoResponse getStorageDirInfo(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructStorageDirInfoRequest(remoteRequest, storageDirDTO);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, SubvolumeInfoResponse.class);
    }

    /**
     * Get the subvolume authorized list
     */
    public List<SubvolumeAuthListResponse> getStorageDirAuthList(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructStorageDirAuthListRequest(remoteRequest, storageDirDTO);
        JSONArray subvolumeLsResponses = commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, JSONArray.class);
        return SubvolumeAuthListResponse.parseResponseToObject(subvolumeLsResponses);
    }

    /**
     * Allow a cephx auth ID access to a subvolume
     */
    public RemoteResponse storageDirAuthorize(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructStorageDirAuthorizeRequest(remoteRequest, storageDirDTO);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * Create a CephFS subvolume in a volume
     */
    public RemoteResponse createStorageDir(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructCreateStorageDirRequest(remoteRequest, storageDirDTO);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * Deny a cephx auth ID access to a subvolume
     */
    public RemoteResponse storageDirDeAuthorize(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructStorageDirDeAuthorizeRequest(remoteRequest, storageDirDTO);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * Delete a CephFS subvolume in a volume
     */
    public RemoteResponse removeStorageDir(RemoteRequest remoteRequest, StorageDirDTO storageDirDTO) throws Throwable {
        CommandDirector.constructRemoveStorageDirRequest(remoteRequest, storageDirDTO);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

}
