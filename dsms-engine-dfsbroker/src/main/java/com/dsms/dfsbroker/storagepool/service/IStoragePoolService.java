/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dsms.common.model.PageParam;
import com.dsms.dfsbroker.node.model.dto.NodeDto;
import com.dsms.dfsbroker.storagepool.model.StoragePool;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolCreateDTO;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolDeleteDTO;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolDiskManageDTO;
import com.dsms.dfsbroker.storagepool.model.dto.StoragePoolNodeManageDTO;

import java.util.List;

public interface IStoragePoolService {
    /**
     * get dsms-storage single storagePool info
     *
     * @return storagePool
     */
    StoragePool get(String poolName);

    /**
     * get dsms-storage all storagePool info
     *
     * @return storagePool info list
     */
    List<StoragePool> list();

    /**
     * get dsms-storage all storagePool info with query
     *
     * @return storagePool info list
     */
    Page<StoragePool> list(PageParam pageParam);

    /**
     * create a task to create a storage pool
     *
     * @return boolean
     */
    boolean create(StoragePoolCreateDTO storagePoolCreateDTO);

    /**
     * list storage pool's all used disk and node
     *
     * @return the list of NodeDto
     */
    List<NodeDto> listUsedDisk(String PoolName);

    /**
     * list storage pool's unused disk on one node
     *
     * @return NodeDto
     */
    NodeDto listUnusedDisk(String poolName, String nodeName);

    /**
     * create add node for storage pool task
     *
     * @return boolean
     */
    boolean addNode(StoragePoolNodeManageDTO storagePoolNodeManageDTO);

    /**
     * create remove node for storage pool task
     *
     * @return boolean
     */
    boolean removeNode(StoragePoolNodeManageDTO storagePoolNodeManageDTO);

    /**
     * create add disk for storage pool on one node task
     *
     * @return boolean
     */
    boolean addDisk(StoragePoolDiskManageDTO storagePoolDiskManageDto);

    /**
     * create remove node for storage pool on one node task
     *
     * @return boolean
     */
    boolean removeDisk(StoragePoolDiskManageDTO storagePoolDiskManageDTO);

    /**
     * create task of delete storage pool
     *
     * @return boolean
     */
    boolean deletePool(StoragePoolDeleteDTO storagePoolDeleteDTO);

    /**
     * validate if the pool is available
     * according to the default fault domain (host) rule of dsms, the crushOsdNum of the storage pool (the number of copies in the replica pool, the number of k+m in the erasure pool), and the use of osd in the storage pool in the crushmap to judge
     *
     * @return boolean true if it is available
     */
    boolean isPoolAvailable(String poolName);


    /**
     * remove all objects from poolName without removing it
     *
     * @param poolName purge pool name
     * @return boolean
     */
    boolean purgePool(String poolName);
}
