/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoragePoolDiskManageDTO {

    @NotBlank(message = "存储池名称不能为空")
    private String poolName;

    private Osd osd;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Osd {
        @ApiModelProperty(value = "osdId列表", example = "[0,1,2]")
        List<Integer> osdIds;
        @ApiModelProperty(value = "osd所在节点名称", example = "node1")
        private String nodeName;
    }
}
