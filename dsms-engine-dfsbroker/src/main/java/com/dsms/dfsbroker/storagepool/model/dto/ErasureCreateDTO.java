/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.storagepool.model.dto;

import com.dsms.dfsbroker.osd.ecprofile.model.dto.EcProfileDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/*
    the dto of create erasure storage pool
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class ErasureCreateDTO extends StoragePoolCreateDTO {
    private EcProfileDto ecProfileDto;

    public ErasureCreateDTO(String poolName, int poolType, Integer pgNum, Integer pgpNum, EcProfileDto ecProfileDto) {
        super(poolName, poolType, pgNum, pgpNum);
        this.ecProfileDto = ecProfileDto;
    }

}
