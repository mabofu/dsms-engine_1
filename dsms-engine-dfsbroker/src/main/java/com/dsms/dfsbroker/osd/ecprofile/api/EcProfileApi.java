/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.ecprofile.api;

import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.dfsbroker.common.CommandDirector;
import com.dsms.dfsbroker.common.api.CommonApi;
import com.dsms.dfsbroker.osd.ecprofile.model.dto.EcProfileDto;
import com.dsms.dfsbroker.osd.ecprofile.model.remote.RemoteEcProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EcProfileApi {
    public static final int MAX_RETRIES = CommonApi.DEFAULT_RETRY_TIMES;

    @Autowired
    private CommonApi commonApi;

    /**
     * create ec-profile in cluster
     *
     * @return RemoteResponse
     */
    public RemoteResponse createEcProfile(RemoteRequest remoteRequest, EcProfileDto ecProfile) throws Throwable {
        CommandDirector.constructCreateEcProfileRequest(remoteRequest, ecProfile);
        return commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
    }

    /**
     * get ec-profile in cluster by name
     *
     * @return RemoteResponse
     */
    public RemoteEcProfile getEcProfile(RemoteRequest remoteRequest, String ecProfileName) throws Throwable {
        CommandDirector.constructGetEcProfileRequest(remoteRequest, ecProfileName);
        return commonApi.remoteCallRequest(remoteRequest, MAX_RETRIES, RemoteEcProfile.class);
    }

    /**
     * get the result of create ec-profile in cluster
     *
     * @return RemoteResponse
     */
    public RemoteResponse getCreateEcProfileResult(RemoteRequest remoteRequest, String requestId) throws Throwable {
        CommandDirector.constructGetRequest(remoteRequest, requestId);
        RemoteResponse remoteResponse = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        return remoteResponse;
    }

    /**
     * delete ec-profile in cluster
     *
     * @return RemoteResponse
     */
    public RemoteResponse deleteEcProfile(RemoteRequest remoteRequest, String ecProfileName) throws Throwable {
        CommandDirector.constructDeleteEcProfileRequest(remoteRequest, ecProfileName);
        RemoteResponse remoteResponse = commonApi.remoteCall(remoteRequest, MAX_RETRIES, RemoteResponse.class);
        return commonApi.getRequestResult(remoteRequest, MAX_RETRIES, remoteResponse.getId());
    }
}
