/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.osd.osd.request;

import com.alibaba.fastjson2.JSON;
import com.dsms.common.constant.CrushFailureDomainEnum;
import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.common.util.ObjectUtil;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;

import java.util.HashMap;
import java.util.Map;

public class UpdateOsdBucketRequest extends RemoteRequest {

    public static final int RETRY_TIMES = 3;
    public static final int RETRY_TIME_SECONDS = 5;
    public static final float DEFAULT_WEIGHT = 1.0F;
    public static final String UPDATE_SUCCESS = "set item id %s name 'osd.%s' weight 1 at location {host=%s} to crush map";

    public UpdateOsdBucketRequest(int osdId, CrushFailureDomainEnum crushFailureDomainEnum, String targetBucket) {
        if (ObjectUtil.checkObjFieldContainNull(osdId)) {
            throw new IllegalArgumentException("osd id must not be blank");
        }

        if (ObjectUtil.checkObjFieldContainNull(crushFailureDomainEnum)) {
            throw new IllegalArgumentException("crush failure domain must not be blank");
        }

        if (ObjectUtil.checkObjFieldContainNull(targetBucket)) {
            throw new IllegalArgumentException("the target must not be blank");
        }

        Map<String, Object> requestParam = new HashMap<>(8);
        requestParam.put("prefix", "osd crush set");
        requestParam.put("id", osdId);
        requestParam.put("weight", DEFAULT_WEIGHT);
        requestParam.put("args", new String[]{
                crushFailureDomainEnum.getType() + "=" + targetBucket
        });

        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix());
        this.setHttpMethod(HttpMethod.POST);
        this.setRequestBody(JSON.toJSONString(requestParam));
    }
}
