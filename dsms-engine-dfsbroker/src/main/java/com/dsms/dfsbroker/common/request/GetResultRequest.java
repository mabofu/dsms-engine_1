/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.dfsbroker.common.request;

import com.dsms.common.remotecall.model.RemoteRequest;
import com.dsms.dfsbroker.common.RequestUrlEnum;
import org.springframework.http.HttpMethod;
import org.springframework.util.ObjectUtils;

public class GetResultRequest extends RemoteRequest {
    public GetResultRequest(String requestId) {
        if (ObjectUtils.isEmpty(requestId)) {
            throw new IllegalArgumentException("requestId must not be blank");
        }
        this.setUrlPrefix(RequestUrlEnum.REQUEST_URL.getUrlPrefix() + "/" + requestId);
        this.setHttpMethod(HttpMethod.GET);
    }
}
