/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.util;

import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.json.JSONUtil;
import com.dsms.common.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;


/**
 * system util
 */
@Slf4j
public class SystemUtil {

    public static void responseUtils(HttpServletRequest request, HttpServletResponse response, Result result) {
        String content = JSONUtil.toJsonStr(result);
        response.setStatus(result.getStatus());
        response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
        ServletUtil.write(response, content, MediaType.APPLICATION_JSON_VALUE);
    }


}
