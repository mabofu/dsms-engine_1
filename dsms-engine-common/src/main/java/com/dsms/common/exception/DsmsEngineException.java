/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.dsms.common.exception;


import com.dsms.common.constant.ResultCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class DsmsEngineException extends RuntimeException {

    /**
     * service status code
     */
    private Integer code;
    private Object data;

    public DsmsEngineException(ResultCode code) {
        super(code.getMessage());
        this.code = code.getCode();
    }

    public DsmsEngineException(Throwable cause, ResultCode code) {
        super(code.getMessage(), cause);
        this.code = code.getCode();
    }

    public DsmsEngineException(Throwable cause, Integer errorCode, String errorMessage) {
        super(errorMessage, cause);
        this.code = errorCode;
    }

    public DsmsEngineException(Throwable cause, Integer errorCode, String errorMessage, Object data) {
        super(errorMessage, cause);
        this.code = errorCode;
        this.data = data;
    }

    public static DsmsEngineException exceptionWithThrowable(Throwable t, ResultCode defaultResult) {
        String errorMessage = defaultResult.getMessage() + ":" + t.getMessage();
        return new DsmsEngineException(t, ResultCode.SERVER_ERROR.getCode(), errorMessage);
    }

    public static DsmsEngineException exceptionWithMessage(String message, ResultCode defaultResult) {
        String errorMessage = defaultResult.getMessage() + ":" + message;
        return new DsmsEngineException(null, ResultCode.SERVER_ERROR.getCode(), errorMessage);
    }

    public static DsmsEngineException exceptionWithMessageAndData(String message, ResultCode defaultResult, Object data) {
        String errorMessage = defaultResult.getMessage() + ":" + message;
        return new DsmsEngineException(null, ResultCode.SERVER_ERROR.getCode(), errorMessage, data);
    }


}
