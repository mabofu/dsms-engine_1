/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

/**
 * Result Code
 */
public enum ResultCode {
    /**
     * HTTP standard status code
     */
    OK(200, "请求成功"),
    //a request that contains a syntax error
    BAD_REQUEST(400, "客户端错误"),
    UNAUTHORIZED(401, "账号未登录"),
    FORBIDDEN(403, "帐号未授权"),
    NOT_FOUND(404, "资源未找到"),
    SERVER_ERROR(500, "服务器异常"),

    /**
     * service status code
     */
    SUCCESS(0, "成功"),
    REMOTE_CALL_FAIL(100, "调用分布式文件系统失败"),
    DATABASE_ERROR(101, "数据库连接异常"),
    SOCKET_ERROR(102, "网络连接异常"),
    REMOTE_CALL_TIMEOUT(103, "调用分布式文件系统超时"),
    REMOTE_CALL_UNKNOWN(104, "调用分布式文件系统异常"),

    LOGIN_EMPTY(1000, "用户名或密码为空"),
    LOGIN_FAIL(1001, "用户名密码错误"),
    LOGIN_FREEZE(1002, "用户被冻结"),
    LOGIN_TIMEOUT(1003, "用户登录已过期"),

    CLUSTER_NOTBIND(1100, "集群未关联"),
    CLUSTER_ADDRESS_NET_ERROR(1101, "集群服务地址网络异常"),
    CLUSTER_SERVICEERROR(1102, "集群存储服务异常"),
    CLUSTER_KEY_ERROR(1103, "集群存储服务认证失败"),
    CLUSTER_ALREADYBIND(1104, "集群已关联"),
    CLUSTER_ADMIN_KEY_ERROR(1105, "获取集群admin密钥失败"),
    CLUSTER_FSID_ERROR(1106, "获取集群fsid失败"),

    NODE_ADDOSD_TASKERROR(1200, "节点新增磁盘任务创建失败"),
    NODE_POOLEXIST(1201, "磁盘已占用，请先移除占用"),
    NODE_REMOVEOSD_TASKERROR(1202, "节点删除磁盘任务创建失败"),
    NODE_LISTNODE_ERROR(1203, "获取节点池信息失败"),
    NODE_NOT_EXIST(1204, "节点不存在"),
    NODE_DEVICE_NOT_AVAILABLE(1205, "磁盘不可用"),
    NODE_OSD_NOT_EXIST(1206, "osd不存在"),
    NODE_REMOVE_OSD_TASK_EXIST(1207, "节点移除磁盘任务已存在"),
    NODE_ADD_OSD_TASK_EXIST(1208, "节点管理磁盘任务已存在"),
    NODE_MOVE_OSD_TO_DEFAULT_ERROR(1209, "移动osd至默认位置失败"),
    NODE_OSD_IS_USED(1210, "磁盘已被存储池使用"),
    NODE_OSD_REMOVE_ERROR(1211, "移除osd失败"),


    POOL_LISTPOOL_ERROR(1300, "获取存储池信息失败"),
    POOL_GETPOOLCAPACITY_ERROR(1301, "获取存储池容量信息失败"),
    POOL_GETFILESYSTEM_ERROR(1302, "获取存储池文件系统使用信息失败"),
    POOL_CREATEPOOL_ERROR(1303, "创建存储池失败"),
    POOL_DISKUSEDD_ERROR(1304, "磁盘已被其余存储池使用"),
    POOL_ADDDISK_ERROR(1305, "新增磁盘失败"),
    POOL_DISKNOTEXIST_ERROR(1306, "磁盘不存在"),
    POOL_REMOVEDISK_ERROR(1307, "移除磁盘失败"),
    POOL_ADDNODE_ERROR(1308, "新增节点失败"),
    POOL_REMOVENODE_ERROR(1309, "移除节点失败"),
    POOL_DELETE_ERROR(1310, "删除存储池失败"),
    POOL_UNAVAILABLE_ERROR(1311, "存储池不可用"),
    POOL_OSD_PG_NOT_EMPTY(1312, "添加磁盘的pg数不为0"),
    POOL_ALREADY_IN_USE(1313, "存储池正在使用中"),

    FS_LIST_ERROR(1400, "获取文件系统信息失败"),
    FS_CREATE_TASK_EXIST(1401, "新增文件系统任务已存在"),
    FS_REMOVE_TASK_EXIST(1402, "删除文件系统任务已存在"),
    FS_CREATE_TASK_ERROR(1403, "新增文件系统任务创建失败"),
    FS_REMOVE_TASK_ERROR(1404, "删除文件系统任务创建失败"),
    STORAGE_DIR_LIST_ERROR(1405, "获取存储目录信息失败"),
    STORAGE_DIR_CREATE_TASK_EXIST(1406, "新增存储目录信息任务已存在"),
    STORAGE_DIR_CREATE_TASK_ERROR(1407, "新增存储目录信息任务创建失败"),
    STORAGE_DIR_REMOVE_TASK_EXIST(1408, "删除存储目录信息任务已存在"),
    STORAGE_DIR_REMOVE_TASK_ERROR(1409, "删除存储目录信息任务创建失败"),
    STORAGE_DIR_ALREADY_EXIST(1410, "存储目录已存在"),

    RBD_GETRBDINFO_ERROR(1500, "获取存储卷信息失败"),
    RBD_CREATE_TASK_ERROR(1501, "新增存储卷任务创建失败"),

    MON_GETCLUSTERSTATUS_ERROR(1600, "获取集群状态失败"),
    MON_OPENRBDMON_ERROR(1601, "开启存储卷性能监控功能失败"),
    MON_POOLALLREADEENABLED_ERROR(1602, "存储池已经开启对存储卷的监控"),
    MON_GETENABLEDPOOLERROR_ERROR(1603, "获取已开启存储卷监控失败"),
    MON_RBDMONITORDISABLED_ERROR(1604, "存储卷未开启监控特性"),

    ALERT_APPRISE_TYPE_ERROR(1700, "告警通知配置类型不存在"),
    ALERT_APPRISE_PARAM_ERROR(1701, "告警通知配置参数异常"),
    EMAIL_HOST_EMPTY(1702, "邮件配置域名不能为空"),
    EMAIL_PORT_EMPTY(1703, "邮件配置端口不能为空"),
    EMAIL_USERNAME_EMPTY(1704, "邮件配置用户名不能为空"),
    EMAIL_PASSWORD_EMPTY(1705, "邮件配置用户密码不能为空"),
    EMAIL_SENDER_EMPTY(1706, "邮件配置发送人不能为空"),
    SMS_PROVIDER_EMPTY(1707, "短信配置平台不能为空"),
    SMS_ACCESS_ID_EMPTY(1708, "短信配置平台密钥id不能为空"),
    SMS_ACCESS_SECRET_EMPTY(1709, "短信配置平台密钥key不能为空"),
    SMS_TEMPLATE_CODE_EMPTY(1710, "短信配置平台模板不能为空"),
    SMS_SIGN_EMPTY(1711, "短信配置平台签名不能为空"),
    ALERT_APPRISE_EMAIL_TEST_ERROR(1712, "发送测试邮件失败"),
    ALERT_APPRISE_SMS_TEST_ERROR(1713, "发送测试短信失败"),
    ALERT_RULE_FIELD_ERROR(1714, "告警规则参数设置错误"),
    ALERT_CONTACT_EXIST(1715, "告警通知人已存在"),
    ALERT_CONTACT_EMAIL_ILLEGAL(1716, "告警通知人邮箱格式验证失败"),
    ALERT_CONTACT_MOBILE_ILLEGAL(1717, "告警通知人手机号格式验证失败"),
    SMS_PROVIDER_HOST_EMPTY(1718, "短信配置平台域名不能为空"),
    SMS_REGION_EMPTY(1719, "短信配置平台地域ID不能为空"),
    SMS_APP_ID_EMPTY(1720, "短信配置平台应用ID不能为空"),


    TASK_EXIST(1800, "任务已存在，请勿重复添加"),

    ALERT_MESSAGE_DELETE_ERROR(1900, "删除告警信息失败"),
    ALERT_MESSAGE_CONFIRM_ERROR(1900, "确认告警信息失败");

    /**
     * response code
     */
    private int code;
    /**
     * response message
     */
    private String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}

