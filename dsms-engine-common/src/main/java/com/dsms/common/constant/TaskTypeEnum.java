/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * task type enum
 */
@Getter
@AllArgsConstructor
public enum TaskTypeEnum {

    /**
     * node add osd task
     */
    ADD_OSD(TypeConstants.ADD_OSD, "节点管理磁盘"),
    /**
     * node remove osd task
     */
    REMOVE_OSD(TypeConstants.REMOVE_OSD, "节点移除磁盘"),
    /**
     * create pool task
     */
    CREATE_POOL(TypeConstants.CREATE_POOL, "创建存储池"),
    /**
     * delete pool task
     */
    DELETE_POOL(TypeConstants.DELETE_POOL, "删除存储池"),
    /**
     * pool add node task
     */
    POOL_ADD_NODE(TypeConstants.POOL_ADD_NODE, "存储池添加节点"),
    /**
     * pool remove node task
     */
    POOL_REMOVE_NODE(TypeConstants.POOL_REMOVE_NODE, "存储池移除节点"),
    POOL_ADD_DISK(TypeConstants.POOL_ADD_DISK, "存储池添加磁盘"),
    POOL_REMOVE_DISK(TypeConstants.POOL_REMOVE_DISK, "存储池移除磁盘"),
    CREATE_FS(TypeConstants.CREATE_FS, "创建文件系统"),
    REMOVE_FS(TypeConstants.REMOVE_FS, "删除文件系统"),
    CREATE_STORAGE_DIR(TypeConstants.CREATE_STORAGE_DIR, "创建存储目录"),
    DELETE_STORAGE_DIR(TypeConstants.DELETE_STORAGE_DIR, "删除存储目录"),
    CREATE_RBD(TypeConstants.CREATE_RBD, "创建存储卷"),
    DELETE_RBD(TypeConstants.DELETE_RBD, "删除存储卷"),
    UPDATE_RBD(TypeConstants.UPDATE_RBD, "存储卷扩缩容");

    private final String type;
    private final String name;


    public static final class TypeConstants {
        private TypeConstants() {
        }

        public static final String ADD_OSD = "add_osd";
        public static final String REMOVE_OSD = "remove_osd";
        public static final String CREATE_POOL = "create_pool";
        public static final String DELETE_POOL = "delete_pool";
        public static final String POOL_ADD_NODE = "pool_add_node";
        public static final String POOL_REMOVE_NODE = "pool_remove_node";
        public static final String POOL_ADD_DISK = "pool_add_disk";
        public static final String POOL_REMOVE_DISK = "pool_remove_disk";
        public static final String CREATE_FS = "create_fs";
        public static final String REMOVE_FS = "remove_fs";
        public static final String CREATE_STORAGE_DIR = "create_storage_dir";
        public static final String DELETE_STORAGE_DIR = "delete_storage_dir";
        public static final String CREATE_RBD = "create_rbd";
        public static final String DELETE_RBD = "delete_rbd";
        public static final String UPDATE_RBD = "update_rbd";
    }

}
