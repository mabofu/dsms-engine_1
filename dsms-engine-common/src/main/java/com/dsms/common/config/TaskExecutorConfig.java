/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
public class TaskExecutorConfig {

    @Bean("dsmsExecutor")
    public ThreadPoolTaskExecutor getTaskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // set core threads number
        executor.setCorePoolSize(4);
        // set max threads number
        executor.setMaxPoolSize(4);
        // set the queue capacity
        executor.setQueueCapacity(4);
        // set thread active time (seconds)
        executor.setKeepAliveSeconds(60);
        // set the thread name prefix
        executor.setThreadNamePrefix("dsms-engine-task-");
        // set rejected policy
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // wait for all tasks to complete before closing the thread pool
        executor.setWaitForTasksToCompleteOnShutdown(true);
        return executor;
    }
}
