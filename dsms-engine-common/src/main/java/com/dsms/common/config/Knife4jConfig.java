/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * swagger config
 */
@Configuration
@EnableSwagger2
public class Knife4jConfig {

    @Bean(value = "dockerBean")
    public Docket dockerBean() {
        // use the Swagger2 specification
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        //can use Markdown
                        .description("# 分布式文件系统管理平台后端RESTful APIs")
                        .termsOfServiceUrl("https://doc.dsms-info.com/")
                        .version("1.0")
                        .build())
                //group name
                .groupName("后端接口")
                .select()
                //specify the path of the Controller scan package
                .apis(RequestHandlerSelectors.basePackage("com.dsms"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

}
