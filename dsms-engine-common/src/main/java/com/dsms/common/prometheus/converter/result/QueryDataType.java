/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.prometheus.converter.result;

import com.dsms.common.constant.MetricConst;
import com.dsms.common.constant.PrometheusInfResultConst;
import com.dsms.common.prometheus.converter.Data;
import com.google.gson.stream.JsonReader;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
public enum QueryDataType {
    Matrix {
        @SuppressWarnings("unchecked")
        @Override
        public MatrixData convert(JsonReader reader) throws IOException {
            MatrixData resultDataItem = new MatrixData();
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (MetricConst.METRIC.equalsIgnoreCase(name)) {
                    Map<String, String> metric = new HashMap<>();
                    reader.beginObject();
                    while (reader.hasNext()) {
                        metric.put(reader.nextName(), reader.nextString());
                    }
                    reader.endObject();
                    resultDataItem.setMetric(metric);
                } else if (MetricConst.VALUES.equalsIgnoreCase(name)) {
                    ArrayList<QueryResultItemValue> resultDataItemValue = new ArrayList<>();
                    reader.beginArray();
                    while (reader.hasNext()) {
                        reader.beginArray();
                        double timeStamp = 0;
                        double value;
                        try {
                            timeStamp = reader.nextDouble();
                            value = reader.nextDouble();
                            resultDataItemValue.add(new QueryResultItemValue(timeStamp, value));
                        } catch (NumberFormatException e) {
                            String currentValue = reader.nextString();
                            log.warn("The data returned by prometheus is meaningless：{},{}", currentValue, e.getMessage(), e);
                            if (Objects.equals(currentValue, PrometheusInfResultConst.POSITIVE_INFINITY)) {
                                resultDataItemValue.add(new QueryResultItemValue(timeStamp, Double.POSITIVE_INFINITY));
                            } else if (Objects.equals(currentValue, PrometheusInfResultConst.NEGATIVE_INFINITY)) {
                                resultDataItemValue.add(new QueryResultItemValue(timeStamp, Double.NEGATIVE_INFINITY));
                            } else {
                                resultDataItemValue.add(new QueryResultItemValue(timeStamp, Double.NaN));
                            }
                        }
                        reader.endArray();
                    }
                    reader.endArray();
                    resultDataItem.setDataValues(resultDataItemValue.toArray(new QueryResultItemValue[]{}));
                }
            }
            reader.endObject();
            return resultDataItem;
        }

    },

    Vector {
        @SuppressWarnings("unchecked")
        @Override
        public VectorData convert(JsonReader reader) throws IOException {
            VectorData resultDataItem = new VectorData();
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if ("metric".equalsIgnoreCase(name)) {
                    Map<String, String> metric = new HashMap<>();
                    reader.beginObject();
                    while (reader.hasNext()) {
                        metric.put(reader.nextName(), reader.nextString());
                    }
                    reader.endObject();
                    resultDataItem.setMetric(metric);
                } else if ("value".equalsIgnoreCase(name)) {
                    reader.beginArray();
                    double timeStamp = 0;
                    double value;
                    try {
                        timeStamp = reader.nextDouble();
                        value = reader.nextDouble();
                        resultDataItem.setDataValue(new QueryResultItemValue(timeStamp, value));
                    } catch (NumberFormatException e) {
                        String currentValue = reader.nextString();
                        log.warn("The data returned by prometheus is meaningless：{},{}", currentValue, e.getMessage(), e);
                        if (Objects.equals(currentValue, PrometheusInfResultConst.POSITIVE_INFINITY)) {
                            resultDataItem.setDataValue(new QueryResultItemValue(timeStamp, Double.POSITIVE_INFINITY));
                        } else if (Objects.equals(currentValue, PrometheusInfResultConst.NEGATIVE_INFINITY)) {
                            resultDataItem.setDataValue(new QueryResultItemValue(timeStamp, Double.NEGATIVE_INFINITY));
                        } else {
                            resultDataItem.setDataValue(new QueryResultItemValue(timeStamp, Double.NaN));
                        }
                    }
                    reader.endArray();
                }
            }
            reader.endObject();
            return resultDataItem;
        }

    };

    public abstract <T extends Data> T convert(JsonReader reader) throws IOException;
}
