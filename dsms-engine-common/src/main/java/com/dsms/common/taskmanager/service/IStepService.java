/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.taskmanager.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.dsms.common.remotecall.model.RemoteResponse;
import com.dsms.common.taskmanager.model.Step;

import java.util.List;

/**
 * Step operation service
 */
public interface IStepService extends IService<Step> {

    public Step getStepResponse(Step step, RemoteResponse executeResponse) throws Throwable;

    public Step getStepResponse(Step step, RemoteResponse executeResponse, List<String> successMsg, String errMsg) throws Throwable;

}
