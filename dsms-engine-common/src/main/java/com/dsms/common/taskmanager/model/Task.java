/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.taskmanager.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dsms.common.constant.TaskExclusiveEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Task table
 */
@Data
@ApiModel(value = "Task对象", description = "任务信息表")
@NoArgsConstructor
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("任务名称")
    private String taskName;

    @ApiModelProperty("任务类型")
    private String taskType;

    @ApiModelProperty("任务状态（0-未开始，1-队列中，2-执行中，3-完成，4-失败）")
    private Integer taskStatus;

    @ApiModelProperty("任务信息")
    private String taskMessage;

    @ApiModelProperty("任务参数")
    private String taskParam;

    @ApiModelProperty("任务开始时间")
    private LocalDateTime taskStartTime;

    @ApiModelProperty("任务结束时间")
    private LocalDateTime taskEndTime;

    @ApiModelProperty("任务错误信息")
    private String taskErrorMessage;

    @ApiModelProperty("是否为独占任务（0-非独占，1-独占）")
    private int isExclusive;

    public Task(String taskName, String taskType, Integer taskStatus, String taskMessage, String taskParam) {
        this.taskName = taskName;
        this.taskType = taskType;
        this.taskStatus = taskStatus;
        this.taskMessage = taskMessage;
        this.taskParam = taskParam;
        this.isExclusive = TaskExclusiveEnum.NON_EXCLUSIVE.getCode();
    }

    public Task(String taskName, String taskType, Integer taskStatus, String taskMessage, String taskParam, int isExclusive) {
        this.taskName = taskName;
        this.taskType = taskType;
        this.taskStatus = taskStatus;
        this.taskMessage = taskMessage;
        this.taskParam = taskParam;
        this.isExclusive = isExclusive;
    }
}
