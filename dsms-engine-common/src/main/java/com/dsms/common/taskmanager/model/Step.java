/*
 *    Copyright 2022 The DSMS Authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.dsms.common.taskmanager.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Task step class
 */
@Data
@ApiModel(value = "Step对象", description = "子任务信息表")
@NoArgsConstructor
@AllArgsConstructor
public class Step implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("主任务id")
    private Integer taskId;

    @ApiModelProperty("子任务名称")
    private String stepName;

    @ApiModelProperty("子任务类型")
    private String stepType;

    @ApiModelProperty("子任务状态（0-未开始，1-队列中，2-执行中，3-完成，4-失败）")
    private Integer stepStatus;

    @ApiModelProperty("子任务信息")
    private String stepMessage;

    @ApiModelProperty("子任务参数")
    private String stepParam;

    @ApiModelProperty("子任务开始时间")
    private LocalDateTime stepStartTime;

    @ApiModelProperty("子任务结束时间")
    private LocalDateTime stepEndTime;

    @ApiModelProperty("子任务错误信息")
    private String stepErrorMessage;

    @TableField(exist = false)
    @ApiModelProperty("子任务成功信息")
    private String successMessage;

    public Step(Integer taskId, String stepName, String stepType, String stepMessage, Integer stepStatus) {
        this.taskId = taskId;
        this.stepName = stepName;
        this.stepType = stepType;
        this.stepMessage = stepMessage;
        this.stepStatus = stepStatus;
    }

    public Step(Integer taskId, String stepName, String stepType, String stepMessage, Integer stepStatus, String stepParam) {
        this.taskId = taskId;
        this.stepName = stepName;
        this.stepType = stepType;
        this.stepMessage = stepMessage;
        this.stepStatus = stepStatus;
        this.stepParam = stepParam;
    }

}
