# dsms-engine

### 介绍

dsms-engine是DSMS （Distributed Storage Management System）的管理引擎组件，实现对分布式存储的管理。

本组件可以检查和管理分布式存储中的各方面资源，如节点管理、存储池管理、文件系统管理等。

### 技术栈

| 框架              | 说明            | 版本      |
|-----------------|---------------|---------|
| JAVA            | 应用开发语言        | 11      |
| Spring Boot     | 应用开发框架        | 2.7.5   | 
| Spring MVC      | MVC 框架        | 5.3.23  |
| Spring Security | Spring 安全框架   | 5.7.4   |
| Quartz          | 任务调度组件        | 2.3.2   |
| logback         | 日志组件          | 1.2.11  |
| MyBatis Plus    | MyBatis 增强工具包 | 3.5.2   |
| Druid           | 数据库连接池        | 1.2.15  |
| Mariadb         | 关系型数据库        | 10.6    |                                                                
| Redis           | key-value数据库  | 6.2.6   | 
| Knife4j         | Swagger 增强框架  | 3.0.3   |
| Lombok          | Java 开发工具库    | 1.18.24 | 
| JUnit 5         | Java 单元测试框架   | 5.8.2   |
| Mockito         | Java Mock 框架  | 4.5.1   |

### 功能模块

1. 集群管理模块
2. 节点管理模块
3. 存储池管理模块
4. 存储卷管理模块
5. 监控中心模块
6. 告警管理模块
7. 系统管理模块

### 参与贡献

你可以[提一个 issue](https://gitee.com/anolis/dsms-engine/issues/new) 或者提交一个 Pull Request。
**Pull Request:**

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request